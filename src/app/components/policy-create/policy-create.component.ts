import { Router } from '@angular/router';
import { ApiService } from './../../service/api.service';
import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: 'app-policy-create',
  templateUrl: './policy-create.component.html',
  styleUrls: ['./policy-create.component.css']
})

export class PolicyCreateComponent implements OnInit {  
  submitted = false;
  policyForm: FormGroup;
  todayDate: string;
  constructor(
    public fb: FormBuilder,
    private router: Router,
    private ngZone: NgZone,
    private apiService: ApiService
  ) { 
    this.mainForm();
  }

  ngOnInit() {
     this.todayDate = new Date().toISOString().substring(0,10);
   }

  mainForm() {
    this.policyForm = this.fb.group({
      policyNumber: ['', [Validators.required]],
      policyStartDate:['', Validators.required],
      policyEndDate:['', Validators.required],
      policyDescription:['', Validators.required],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      dob: ['', [Validators.required]]
      
    })
  }


  // Getter to access form control
  get myForm(){
    return this.policyForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (!this.policyForm.valid) {
      return false;
    } else {
      let data = this.policyForm.value;
      data['status'] = 'Active';
      console.log(data);
      this.apiService.createPolicy(this.policyForm.value).subscribe(
        (res) => {
          console.log('Policy successfully created!')
          this.ngZone.run(() => this.router.navigateByUrl('/policy-list'))
        }, (error) => {
          console.log(error);
        });
    }
  }

}
