import { Policy } from './../../model/Policy';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { ApiService } from './../../service/api.service';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { DateTime } from "luxon";

@Component({
  selector: 'app-policy-edit',
  templateUrl: './policy-edit.component.html',
  styleUrls: ['./policy-edit.component.css']
})

export class PolicyEditComponent implements OnInit {
  submitted = false;
  editForm: FormGroup;
  policyData: Policy[];  
  todayDate:string;

  constructor(
    public fb: FormBuilder,
    private actRoute: ActivatedRoute,
    private apiService: ApiService,
    private router: Router
  ) {}

  ngOnInit() {
    this.updatePolicy();
    let id = this.actRoute.snapshot.paramMap.get('id');
    this.todayDate = new Date().toISOString().substring(0,10);
    this.getPol(id);
    this.editForm = this.fb.group({
      policyNumber: ['', [Validators.required]],
      policyStartDate:['', Validators.required],
      policyEndDate:['', Validators.required],
      policyDescription:['', Validators.required],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      dob: ['', [Validators.required]]
    })
  }

  

  // Getter to access form control
  get myForm() {
    return this.editForm.controls;
  }

  getPol(id) {
    this.apiService.getPol(id).subscribe(data => {
      console.log(data)
      this.editForm.setValue({
        policyNumber: data['policyNumber'],
        policyStartDate: this.formatDateTime(data['policyStartDate']),
        policyEndDate: this.formatDateTime(data['policyEndDate']),
        policyDescription: data['policyDescription'],
        firstName: data['firstName'],
        lastName: data['lastName'],
        dob: this.formatDateTime(data['dob'])
      });
    });
  }

  updatePolicy() {
    this.editForm = this.fb.group({
      policyNumber: ['', [Validators.required]],
      policyStartDate:['', Validators.required],
      policyEndDate:['', Validators.required],
      policyDescription:['', Validators.required],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      dob: ['', [Validators.required]]
    })
  }

  onSubmit() {
    this.submitted = true;
    if (!this.editForm.valid) {
      return false;
    } else {
      if (window.confirm('Are you sure?')) {
        let id = this.actRoute.snapshot.paramMap.get('id');
        this.apiService.updatePolicy(id, this.editForm.value)
          .subscribe(res => {
            this.router.navigateByUrl('/policy-list');
            console.log('Content updated successfully!')
          }, (error) => {
            console.log(error)
          })
      }
    }
  }
  formatDateTime (date){
    console.log(DateTime.fromISO(date).toFormat('yyyy-MM-dd'))
    return DateTime.fromISO(date).toFormat('yyyy-MM-dd');
  }

}