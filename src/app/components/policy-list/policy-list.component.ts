import { Component, OnInit } from '@angular/core';
import { ApiService } from './../../service/api.service';

@Component({
  selector: 'app-policy-list',
  templateUrl: './policy-list.component.html',
  styleUrls: ['./policy-list.component.css']
})

export class PolicyListComponent implements OnInit {
  
  policy:any = [];

  constructor(private apiService: ApiService) { 
    this.readPolicy();
  }

  ngOnInit() {}

  readPolicy(){
    this.apiService.getPolicy().subscribe((data) => {      
     this.policy = data;
     console.log(this.policy);
    })    
  }

  removePolicy(policy, index) {
    if(window.confirm('Are you sure?')) {
        this.apiService.deletePolicy(policy._id).subscribe((data) => {
          this.policy.splice(index, 1);
        }
      )    
    }
  }

}