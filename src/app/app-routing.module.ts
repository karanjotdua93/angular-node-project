import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PolicyCreateComponent } from './components/policy-create/policy-create.component';
import { PolicyListComponent } from './components/policy-list/policy-list.component';
import { PolicyEditComponent } from './components/policy-edit/policy-edit.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'create-policy' },
  { path: 'create-policy', component: PolicyCreateComponent },
  { path: 'edit-policy/:id', component: PolicyEditComponent },
  { path: 'policy-list', component: PolicyListComponent }  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }