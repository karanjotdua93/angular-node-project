export class Policy {
   policyNumber:string;
   policyStartDate:Date;
   policyEndDate:Date;
   policyDescription:string;
   firstName:string;
   lastName:string;
   dob: Date;
   status: string;
}