const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema
let Policy = new Schema({
   policyNumber: {
      type: String
   },
   firstName: {
      type: String
   },
   lastName: {
      type: String
   },
   policyDescription: {
      type: String
   },
   policyStartDate:{
      type:Date
   },
   policyEndDate:{
      type:Date
   },
   dob:{
      type:Date
   },
   status:{
      type: String
   }
}, {
   collection: 'policies'
})

module.exports = mongoose.model('Policy', Policy)