const express = require('express');
const app = express();
const policyRoute = express.Router();

// Policy model
let Policy = require('../models/Policy');

// Add Policy
policyRoute.route('/create').post((req, res, next) => {
  Policy.create(req.body, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
});

// Get All Policies
policyRoute.route('/').get((req, res) => {
  Policy.find({status:"Active"},(error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})

// Get single Policy
policyRoute.route('/read/:id').get((req, res) => {
  Policy.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})


// Update Policy
policyRoute.route('/update/:id').put((req, res, next) => {
  Policy.findByIdAndUpdate(req.params.id, {
    $set: req.body
  }, (error, data) => {
    if (error) {
      return next(error);
      console.log(error)
    } else {
      res.json(data)
      console.log('Data updated successfully')
    }
  })
})

// Delete Policy
policyRoute.route('/delete/:id').delete((req, res, next) => {
  Policy.findOneAndUpdate(req.params.id, {status: "Inactive"}, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        msg: data
      })
    }
  })
})

module.exports = policyRoute;